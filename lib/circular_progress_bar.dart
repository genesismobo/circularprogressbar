library circular_progress_bar;

import 'package:circular_progress_bar/src/ciruclar_progress_bar_painter.dart';
import 'package:flutter/material.dart';

class CircularProgressBar extends StatefulWidget {
  static const int DURATION = 5000;
  static const double STROKE_WIDTH = 10;
  static const Color COLOR = Colors.blue;
  static const Color BACKGROUND_COLOR = Colors.blueGrey;
  static const double HEIGHT = 100;
  static const double WIDTH = 100;

  int _duration;
  double _backgroundStrokeWidth;
  double _strokeWidth;
  double _height;
  double _width;

  Widget child(int progress) {
    return Text("$progress %");
  }

  bool _showChild;

  AnimationController _progressController;
  AnimationStatusListener _animationStatusListener;
  Animation<double> _animation;
  Color _color;
  Color _backgroundColor;

  CircularProgressBar(
      {int duration,
      double backgroundStrokeWidth,
      double strokeWidth,
      Color color,
      Color backgroundColor,
      double height,
      double width,
      bool showChild,
      AnimationStatusListener animationStatusListener})
      : _duration = duration != null ? duration : DURATION,
        _backgroundStrokeWidth = backgroundStrokeWidth != null
            ? backgroundStrokeWidth
            : STROKE_WIDTH,
        _strokeWidth = strokeWidth != null ? strokeWidth : STROKE_WIDTH,
        _color = color != null ? color : COLOR,
        _backgroundColor =
            backgroundColor != null ? backgroundColor : BACKGROUND_COLOR,
        _height = height != null ? height : HEIGHT,
        _width = width != null ? width : WIDTH,
        _showChild = showChild != null ? showChild : true,
        _animationStatusListener = animationStatusListener;

  void start() {
    _progressController.forward();
  }

  void pause() {
    _progressController.stop(canceled: false);
  }

  void stop() {
    _progressController.reset();
  }

  void restart() {
    stop();
    start();
  }

  void setColor(Color color) {
    this._color = color;
  }

  void setBackgroundColor(Color color) {
    this._backgroundColor = color;
  }

  @override
  _CircularProgressBarState createState() => _CircularProgressBarState();
}

class _CircularProgressBarState extends State<CircularProgressBar>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    widget._progressController = AnimationController(
        vsync: this, duration: Duration(milliseconds: widget._duration));

    if (widget._animationStatusListener != null) {
      widget._progressController
        ..addStatusListener(widget._animationStatusListener);
    }

    widget._animation =
        Tween<double>(begin: 0, end: 100).animate(widget._progressController)
          ..addListener(() {
            setState(() {});
          });
  }

  @override
  void dispose() {
    widget._progressController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget._width,
      height: widget._height,
      child: CustomPaint(
        foregroundPainter: CircularProgressBarPainter(
            widget._animation.value,
            widget._backgroundStrokeWidth,
            widget._strokeWidth,
            widget._color,
            widget._backgroundColor),
        child: widget._showChild
            ? Center(child: widget.child(widget._animation.value.toInt()))
            : Container(),
      ),
    );
  }
}
