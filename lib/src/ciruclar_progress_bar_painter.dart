import 'dart:core';
import 'dart:math';

import 'package:flutter/material.dart';

class CircularProgressBarPainter extends CustomPainter {

  CircularProgressBarPainter(this.progress, this.backgroundStrokeWidth,
      this.strokeWidth, this.color, this.backgroundColor);

  double progress;
  double backgroundStrokeWidth;
  double strokeWidth;
  Color color;
  Color backgroundColor;

  @override
  void paint(Canvas canvas, Size size) {

    Paint backgroundCircle = Paint()
      ..strokeWidth = backgroundStrokeWidth
      ..style = PaintingStyle.stroke
      ..color = backgroundColor;

    Paint progressArc = Paint()
      ..strokeWidth = strokeWidth
      ..style = PaintingStyle.stroke
      ..color = color
      ..strokeCap = StrokeCap.round;

    Offset center = Offset(size.width/2, size.height/2);
    double radiusBackground = min(size.width/2, size.height/2) - backgroundStrokeWidth;
    double radiusArc = min(size.width/2, size.height/2) - strokeWidth;

    canvas.drawCircle(center, radiusBackground, backgroundCircle);

    double angle = 2 * pi * progress/100;
    canvas.drawArc(Rect.fromCircle(center: center, radius: radiusArc), -pi/2, angle, false, progressArc);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }

}